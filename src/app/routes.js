//RUTAS
app.config(["$stateProvider", "$urlRouterProvider", "$injector", "APP_STATES", function($stateProvider, $urlRouterProvider, $injector, APP_STATES) {

	$urlRouterProvider.otherwise( function($injector) {
		var $state = $injector.get("$state");
		$state.go('home');
	});

	$stateProvider.state(APP_STATES.HOME, {

		url: "/",
		templateUrl: "./app/modules/home/homeView.html",
		controller : "homeController",
		controllerAs : "ctrl"

	}).state(APP_STATES.LOGIN, {

		url: "/login",
		templateUrl: "./app/modules/login/loginView.html",
		controller : "loginController",
		controllerAs : "ctrl"

	}).state(APP_STATES.DASHBOARD, {

		url: "/dashboard",
		templateUrl: "./app/modules/dashboard/dashboardView.html",
		controller : "dashboardController",
		controllerAs : "ctrl",
		data : {
			authNeeded : true
		}

	}).state(APP_STATES.PATIENTS, {

		url: "/patients",
		templateUrl: "./app/modules/patients/patientsView.html",
		controller : "patientsController",
		controllerAs : "ctrl",
		data : {
			authNeeded : true
		}
	}).state(APP_STATES.MESSAGES, {

		url: "/messages",
		templateUrl: "./app/modules/messages/messagesView.html",
		controller : "messagesController",
		controllerAs : "ctrl",
		data : {
			authNeeded : true
		}

	}).state(APP_STATES.LOGOUT, {

		url: "/logout",
		controller: ["$scope", "$injector", "authService", function($scope, $injector, authService) {
			var $state = $injector.get("$state");
			console.log("cayo en state logout y se redirige a public.");
			authService.logout();
			$state.go("home");
		}]

	});
	
}]);
