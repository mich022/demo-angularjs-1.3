app.controller("homeController", ["$scope", "newsService", function($scope, newsService){
  var ctrl = this;
  ctrl.news = newsService.getAll();
}])