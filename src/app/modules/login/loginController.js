app.controller("loginController", ["$scope", "APP_STATES", "authService", "$state", function($scope, APP_STATES, authService, $state){
	var ctrl = this;
	ctrl.appStates = APP_STATES;
	
	ctrl.login = function (){
		console.log("ctrl.email : ", ctrl.email);
		console.log("ctrl.password : ", ctrl.password);
		var params = {
				email : ctrl.email,
				password : ctrl.password
		}
		var response = authService.login(params);
		if(response.data && response.data.success){
			var token = response.data.success.token;
			authService.saveToken(token);
			$state.go(APP_STATES.DASHBOARD);
		}
	};
}])