app.constant("API_RESOURCES", (function(){
    
    var url = window.location.href;
    var protocol = window.location.protocol;
    
    //Ambiente productivo
	var API_BASE_URL = protocol+"//domain.subdomain.com/ekv-services";
	
	return {
		API_BASE_URL : API_BASE_URL
	}
})());

app.constant("APP_STATES", (function() {

	var url = window.location.href;
	var protocol = window.location.protocol;
	
	//Ambiente productivo
	var APP_BASE_URL = protocol+"//localhost:8080";
	
	return {
		APP_BASE_URL : APP_BASE_URL,
		HOME : "home",
		LOGIN : "login",
		DASHBOARD : "dashboard",
		PATIENTS : "patients",
		APPOINTMENTS : "appointments",
		APPOINTMENTS_REQUESTS : "appointments-requests",
		MESSAGES : "messages",
		BILLING : "billing",
		LOGOUT : "logout"
	}
	
})());