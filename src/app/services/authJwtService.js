app.factory("authJwtService", ["$window", "$log", function($window, $log) {	
	var service = {};
	service.parseJwt = function (token) {
		var base64Url = token.split(".")[1];
		var base64String = base64Url.replace("-", "+").replace("_", "/");
		return JSON.parse(Base64.decode(base64String));
	};

	service.saveToken = function (token) {
		$window.sessionStorage["jwtToken"] = token;
	};

	service.logout = function () {
		$window.sessionStorage.removeItem("jwtToken");
	};

	service.getToken = function () {
		var token = $window.sessionStorage["jwtToken"];		
		return token;
	};

	service.isAuthed = function () {
		var token = service.getToken();
		if (token) {
			var params = service.parseJwt(token);
			if(params.exp){
				return Math.round(new Date().getTime() / 1000) <= params.exp;
			} else {
				//infinito
				return true;
			}
		} else {
			return false;
		}
	};
	
	service.getName = function () {
		var token = service.getToken();
		var name = null;
		if (token) {
			var params = service.parseJwt(token);
			if(params.name){
				name = params.name;
			}
		}
		return name;
	};
	
    return service;
    
}]);