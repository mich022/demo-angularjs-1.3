app.factory("newsService", [function() {	
	var service = {};
	service.URLResource="";
	
	service.getAll = function(){
		var news = [];
		var noticia = {};
		
		noticia = {
			author : "Zakk Wylde",
			title : "Title of Article 1",
			description :"Lorem A ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat sollicitudin nisi, at convallis nunc semper et. Donec ultrices odio ac purus facilisis, at mollis urna finibus.",
			imgUrl : "./img/doctor1.jpg"
		};
		
		news.push(noticia);
		
		noticia = {
			author : "Ozzyy",
			title : "Title of Article B",
			description :"Lorem B ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat sollicitudin nisi, at convallis nunc semper et. Donec ultrices odio ac purus facilisis, at mollis urna finibus.",
			imgUrl : "./img/doctor2.jpg"
		};
			
		news.push(noticia);
		
		noticia = {
			author : "John Bonham",
			title : "Title of Article 3",
			description :"Lorem C ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat sollicitudin nisi, at convallis nunc semper et. Donec ultrices odio ac purus facilisis, at mollis urna finibus.",
			imgUrl : "./img/doctor3.jpg"
		};
		
		news.push(noticia);
			
		
		return news;
	};
	
	return service;
    
}]);