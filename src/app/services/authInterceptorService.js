app.factory("authInterceptorService", ["authJwtService", "API_RESOURCES", "APP_STATES", "$injector", function(authJwtService, API_RESOURCES, APP_STATES, $injector) {	
	var service = {};
	
	service.request = function(config) {
		var token = AuthJwt.getToken();
//		if(token){
//			config.headers.Authorization = 'Bearer ' + token;
//		}
//		if(config.url.indexOf(API_RESOURCES.API_BASE_URL) === 0 && token) {
//			config.headers.Authorization = 'Bearer ' + token;
//		}
		return config;
	};

//	service.response = function(res) {
//		if(res.config.url.indexOf(API_RESOURCES.API_BASE_URL) === 0 
//				&& res.data && res.data.success && res.data.success.token) { 
//			AuthJwt.saveToken(res.data.success.token);
//			//$log.info("El token de acceso se guardo en sessionStorage exitosamente.");
//		}
//		return res;
//	};
	
	service.responseError = function(rejection) {
		if(rejection.status === 401){
			var $state = $injector.get("$state");
			$state.go(APP_STATES.LOGIN);
		}
	};
	
    return service;
    
}]);