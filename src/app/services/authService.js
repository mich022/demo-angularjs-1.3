app.factory("authService", ["authJwtService", function(authJwtService) {	
	var service = {};
	service.URLResource="";
	
	service.login = function (params) {
		var user = {
			email : params.email,
			password : params.password
		};
		
		var response = {
			data : {
				success : {
					token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
				}
			}	
		}
		
		return response;
		
//		return $http.post(service.URLResource+"/login", user);
	};
	
	service.logout = function (){
		authJwtService.logout();
	};
	
	service.isAuthed = function(){
		return authJwtService.isAuthed() ? authJwtService.isAuthed() : false;
	};
	
	service.saveToken = function (token) {
		authJwtService.saveToken(token);
	};
	
	service.getName = function() {
		return authJwtService.getName();
	};
	
	return service;
    
}]);