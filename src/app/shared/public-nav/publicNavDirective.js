app.directive("publicNav", ["$state", "APP_STATES", function($state, APP_STATES) {
	return {
		templateUrl : "./app/shared/public-nav/publicNavView.html",
	    restrict: 'E',
	    replace: true,
	    scope: true,
		controllerAs : "ctrl",
		controller: ["authService", function(authService){
			var ctrl = this;
			ctrl.appStates = APP_STATES;
			ctrl.currentState = $state.current;
		}]
	};
}]);