app.directive("privateNav", ["$state", "APP_STATES", function($state, APP_STATES) {
	return {
		templateUrl : "./app/shared/private-nav/privateNavView.html",
	    restrict: 'E',
	    replace: true,
	    scope: true,
		controllerAs : "ctrl",
		controller: ["authService", function(authService){
			var ctrl = this;
			ctrl.appStates = APP_STATES;
			ctrl.currentState = $state.current;
		}]
	};
}]);