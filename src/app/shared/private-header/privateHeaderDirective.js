app.directive("privateHeader", [function() {
	return {
		templateUrl : "./app/shared/private-header/privateHeaderView.html",
	    restrict: 'E',
	    replace: true,
	    scope: true,
		controllerAs : "ctrl",
		controller: ["authService", function(authService){
			var ctrl = this;
		}]
	};
}]);