var app = angular.module("demo-angularjs-1.3", ["ui.router", "ui.bootstrap"]);

app.run(["$transitions", "authService", "APP_STATES", function($transitions, authService, APP_STATES){
	
	//Check de states con login requerido
	$transitions.onBefore({}, function(transition) {
		if (transition.to().data 
				&& transition.to().data.authNeeded
				&& ! authService.isAuthed()) {
			return transition.router.stateService.target(APP_STATES.LOGIN);
		}
	});
	
}]);