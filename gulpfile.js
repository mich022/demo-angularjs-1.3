var gulp = require('gulp');
var filter = require('gulp-filter');
var htmlmin = require('gulp-htmlmin');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var gls = require('gulp-live-server');

gulp.task('default', ['js-app-dist', 'js-lib-dist', 'html-dist', 'css-app-dist', 'css-lib-dist', 'img-dist', 'fonts-dist']);

gulp.task('html-dist', function (done) {
  gulp.src([
    './src/**'
  ])
    .pipe(filter('**/*.html'))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist'))
    .on('end', done)
});

gulp.task('css-app-dist', function(done) {
	var dest = "./dist/css/";  
  	var sourceDir = './src/css/';
  	var libFiles = [sourceDir + '/styles.css'];

  	gulp.src(libFiles)
  	.pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ basename : 'demo-angularjs-1.3-app', extname: '.min.css' }))
    .pipe(gulp.dest(dest))
    .on('end', done);
});

gulp.task('css-lib-dist', function(done) {
	var dest = "./dist/css/";
  	var sourceDir = './node_modules';
  	var libFiles = [
			sourceDir + '/bootstrap-only-css/dist/bootstrap.min.css',
  		    sourceDir + '/@neos21/bootstrap3-glyphicons/dist/css/bootstrap3-glyphicons.min.css'
  		];

  	gulp.src(libFiles)
  	.pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(concat('demo-angularjs-1.3-lib.min.css'))
    .pipe(gulp.dest(dest))
    .on('end', done);
});

gulp.task('js-app-dist', function(done) {
  var appDir = './src/app';
  var appFiles = [appDir+'/app.js',
    appDir+'/routes.js',
    appDir+'/constants.js',
    appDir+'/shared/public-header/publicHeaderDirective.js',
    appDir+'/shared/public-nav/publicNavDirective.js',
    appDir+'/shared/public-footer/publicFooterDirective.js',
    appDir+'/shared/private-header/privateHeaderDirective.js',
    appDir+'/shared/private-nav/privateNavDirective.js',
    appDir+'/shared/private-aside/privateAsideDirective.js',
    appDir+'/shared/private-footer/privateFooterDirective.js',
    appDir+'/modules/home/homeController.js',
    appDir+'/modules/login/loginController.js',
    appDir+'/modules/dashboard/dashboardController.js',
    appDir+'/modules/patients/patientsController.js',
    appDir+'/modules/messages/messagesController.js',
    appDir+'/services/authInterceptorService.js',
    appDir+'/services/authJwtService.js',
    appDir+'/services/authService.js',
    appDir+'/services/newsService.js'
  ];

	gulp.src(appFiles)
  	.pipe(concat('demo-angularjs-1.3-app.js'))
  	.pipe(gulp.dest('./dist/scripts/'))
    .pipe(concat('demo-angularjs-1.3-app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/scripts/'))
    .on('end', done)
});

gulp.task('js-lib-dist', function(done) {
  var libDir = './node_modules';
  var libFiles = [libDir + '/angular/angular.min.js',
	libDir + '/angular-ui-router/release/angular-ui-router.min.js',
	libDir + '/angular-ui-bootstrap/ui-bootstrap.min.js',
	libDir + '/js-base64/base64.min.js'];

  gulp.src(libFiles)
    .pipe(concat('demo-angularjs-1.3-lib.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/scripts/'))
    .on('end', done)
});

gulp.task('fonts-dist', function() {
  gulp.src([
	  './node_modules/@neos21/bootstrap3-glyphicons/dist/fonts/**/*'
  ])
    .pipe(gulp.dest('./dist/fonts/'));
});

gulp.task('img-dist', function (done) {
  gulp.src([
    './src/img/**/*'
  ])
    .pipe(gulp.dest('./dist/img'))
    .on('end', done)
});

gulp.task('serve', function() {
  var server = gls.static('dist', 80);
  server.start();
});